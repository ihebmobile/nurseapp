package tn.converto.nurse;

import android.content.Context;
import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;
import io.branch.referral.Branch;

public class CustomApplicationClass extends MultiDexApplication {

  @Override
  public void onCreate() {
    super.onCreate();

    // Branch logging for debugging
    Branch.enableLogging();

    // Branch object initialization
    Branch.getAutoInstance(this);
  }

  @Override
  protected void attachBaseContext(Context base) {
    super.attachBaseContext(base);
    MultiDex.install(this);
  }
}