import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { UiService } from '../services/ui.service';
import { ResetPasswordPage } from '../reset-password/reset-password.page';
import { Events } from '../services/Events';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  loginForm: FormGroup;
  submitAttempt: boolean=false;
  constructor(private events:Events,private ui:UiService,private navCtr:NavController,private auth:AuthService,private formBuilder: FormBuilder,private router:Router) {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.required]
    })
  }

  login(){
    this.submitAttempt = true
    if (this.loginForm.valid) {
      let credencial = this.loginForm.value;
      this.auth.login(credencial).then((res:any)=>{
          let user=res.user;
          console.log(user);
          this.events.publish('user-login',user.id);
          if(user.type=="customer")
          this.navCtr.navigateForward("/type");
        else if(user.type=="nurse"){
          let nurse=user;
           
          this.navCtr.navigateRoot('/reservations', {
            state: {
            nurse: JSON.stringify(nurse),
            type: 'customer'
            }
        });
        }
      }).catch(err=>{
        console.log(err);
      })

    }
  }

  register(){
    this.router.navigate(['/registration'], {
      // state: {
      // nurse: JSON.stringify(nurse),
      // type: 'customer'
      // }
  });
}


showForgot() {
    if (this.loginForm.controls.email['valid']) {
            let credencial = this.loginForm.value;
            this.auth.forgotPassword(credencial).then((res)=>{
              console.log(res);
              this.ui.fireSuccess('Un email de réinitialisation de mot de passe a été envoyé');
            }).catch(err=>{
             this.ui.fireError(err);
            });
    }else
    {
      this.ui.fireError("Merci de saisir un email valide");
    }
}
}

