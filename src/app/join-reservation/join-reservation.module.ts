import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { JoinReservationPageRoutingModule } from './join-reservation-routing.module';

import { JoinReservationPage } from './join-reservation.page';
import { StarRatingModule } from 'ionic5-star-rating';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StarRatingModule,
    JoinReservationPageRoutingModule
  ],
  declarations: [JoinReservationPage]
})
export class JoinReservationPageModule {}
