import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { Plugins } from '@capacitor/core';
import { UiService } from '../services/ui.service';

const { Geolocation } = Plugins;
declare var google;

@Component({
  selector: 'app-join-reservation',
  templateUrl: './join-reservation.page.html',
  styleUrls: ['./join-reservation.page.scss'],
})
export class JoinReservationPage implements OnInit {
  @ViewChild('map') gmap: ElementRef;
  map: any;
  marker: any;
  geocoder: any;
  location: any = {};
  isLoading = false
  mapOptions: any = {
    center: new google.maps.LatLng(36.7948624, 10.0732384),
    zoom: 2,
    disableDefaultUI: true,
    clickableIcons: false
  }
  newLocation: any = {}
  currentlat: number;
  currentlong: number;
  private nurse: any = {};
  private days: any = ["LU", "MA", "ME", "JE", "VE", "SA", "DI"];
  private availability: any = {
    "LU": ["11:30 AM", "11:50 AM"],
    "MA": ["11:30 AM", "10:00 AM", "11:50 AM", "6:00 PM"],
    "ME": ["11:30 AM", "11:50 AM"],
    "JE": ["11:00 AM", "11:30 AM", "11:50 AM"],
    "VE": ["11:30 AM", "12:50 AM"],
    "SA": ["11:30 AM", "9:50 AM"],
    "DI": ["11:30 AM", "11:50 AM"]
  };

  private selectedtime: any = null;
  private selectedIndex: any = null;
  private current_day: number;
  private current = new Date();
  private weekstart = this.current.getDate() - this.current.getDay() + 1;
  private days_numbers: any[] = [];
  private type: any;
  private availables: any;
  private selected_date: any;
  service_id: any;
  private icons = {
    start: new google.maps.MarkerImage(
     // URL
     'http://maps.google.com/mapfiles/kml/pal3/icon23.png',
     // (width,height)
     new google.maps.Size( 44, 32 ),
     // The origin point (x,y)
     new google.maps.Point( 0, 0 ),
     // The anchor point (x,y)
     new google.maps.Point( 22, 32 )
    ),
    end: new google.maps.MarkerImage(
     // URL
     'http://maps.google.com/mapfiles/kml/pal3/icon28.png',
     // (width,height)
     new google.maps.Size( 44, 32 ),
     // The origin point (x,y)
     new google.maps.Point( 0, 0 ),
     // The anchor point (x,y)
     new google.maps.Point( 22, 32 )
    )
   };
  
  constructor(private ui:UiService,private route: ActivatedRoute, private navCtr: NavController, private router: Router, private auth: AuthService) {
    this.nurse = (JSON.parse(this.router.getCurrentNavigation().extras.state.nurse));
    this.selectedIndex = this.router.getCurrentNavigation().extras.state.selectedDay;
    this.availables = this.router.getCurrentNavigation().extras.state.availables;
    this.selected_date = this.router.getCurrentNavigation().extras.state.selected_date;
    this.service_id = this.router.getCurrentNavigation().extras.state.service_id;
    console.log(this.service_id);
    console.log(this.availables);
    console.log(this.selectedIndex);
    let d = new Date();
    this.current_day = d.getDay();
    console.log(this.current_day);
    console.log(this.weekstart);
    let i = 1;
    this.days.forEach(day => {
      this.days_numbers[i - 1] = this.current.getDate() - this.current.getDay() + i;
      i++;
    })
    console.log(this.days_numbers);
  }

  ngOnInit() {
    setTimeout(() => {
      console.log(this.nurse);
    }, 2000);

  }
  goBack() {
    this.navCtr.pop();
  }

  joinReservation() {
    this.auth.joinReservation(this.selected_date, this.availables[this.selectedtime], this.service_id).then((res: any) => {

    }).catch(err => console.log(err));
  }

  ionViewDidEnter() {
    this.mapInitializer()
    this.getCurrentPosition();
  }

  // start Map Methods

startNavigating(lat,lng){
  
  let directionsService = new google.maps.DirectionsService;
  let directionsDisplay = new google.maps.DirectionsRenderer({
    map: this.map,
    suppressMarkers: true
});

  directionsDisplay.setMap(this.map);
  directionsService.route({
      origin: {lat:parseFloat(lat), lng: parseFloat(lng)},
      destination: {lat: parseFloat(this.nurse.location.lat), lng:parseFloat(this.nurse.location.long)},
      travelMode: google.maps.TravelMode['DRIVING']
  }, (res, status) => {
      console.log(res);
      if(status == google.maps.DirectionsStatus.OK){
          directionsDisplay.setDirections(res);
     
          var leg = res.routes[0].legs[0];
          this.makeMarker(leg.start_location, this.icons.start, "title", (this.map));
          this.makeMarker(leg.end_location, this.icons.end, 'title', (this.map));
      } else {
          console.warn(status);
      }

  });

}


 makeMarker(position, icon, title, map) {
  new google.maps.Marker({
      position: position,
      map: map,
      icon: icon,
      title: title
  });
}
  async mapInitializer() {
    this.map = new google.maps.Map(this.gmap.nativeElement, this.mapOptions);
  }



  async getCurrentPosition() {
    //  this.ui.loading('geolocalization');
    try {
      const resp = await Geolocation.getCurrentPosition({enableHighAccuracy: true,maximumAge: 10000, timeout: 10000})
      this.mapOptions.center = new google.maps.LatLng(Number(resp.coords.latitude), Number(resp.coords.longitude))
      this.centerMap()
      this.startNavigating(resp.coords.latitude,resp.coords.longitude);
      // this.ui.unLoading();
    }catch(error) {
      this.ui.fireError('Verifier vos parametre de geolocalisation')
      console.log('Error getting location', error);
      // this.ui.unLoading();
    }
  }
  async centerMap(geocode = true) {
    this.map.setZoom(15)
    this.map.panTo(this.mapOptions.center)

  }
  // End map Mrthods
}
