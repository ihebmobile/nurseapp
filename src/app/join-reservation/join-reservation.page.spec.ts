import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { JoinReservationPage } from './join-reservation.page';

describe('JoinReservationPage', () => {
  let component: JoinReservationPage;
  let fixture: ComponentFixture<JoinReservationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JoinReservationPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(JoinReservationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
