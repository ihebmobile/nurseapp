import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { JoinReservationPage } from './join-reservation.page';

const routes: Routes = [
  {
    path: '',
    component: JoinReservationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class JoinReservationPageRoutingModule {}
