import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'nurses',
    loadChildren: () => import('./nurses/nurses.module').then( m => m.NursesPageModule)
  },
  {
    path: 'nurse-dtails',
    loadChildren: () => import('./nurse-dtails/nurse-dtails.module').then( m => m.NurseDtailsPageModule)
  },
  {
    path: 'reservations',
    loadChildren: () => import('./reservations/reservations.module').then( m => m.ReservationsPageModule)
  },
  {
    path: 'reservation-dtails',
    loadChildren: () => import('./reservation-dtails/reservation-dtails.module').then( m => m.ReservationDtailsPageModule)
  },
  {
    path: 'registration',
    loadChildren: () => import('./registration/registration.module').then( m => m.RegistrationPageModule)
  },
  {
    path: 'join-reservation',
    loadChildren: () => import('./join-reservation/join-reservation.module').then( m => m.JoinReservationPageModule)
  },
  {
    path: 'map',
    loadChildren: () => import('./map/map.module').then( m => m.MapPageModule)
  },
  {
    path: 'type',
    loadChildren: () => import('./type/type.module').then( m => m.TypePageModule)
  },
  {
    path: 'reservation-info',
    loadChildren: () => import('./reservation-info/reservation-info.module').then( m => m.ReservationInfoPageModule)
  },
  {
    path: 'reset-password',
    loadChildren: () => import('./reset-password/reset-password.module').then( m => m.ResetPasswordPageModule)
  },
  {
    path: 'nurse-profile',
    loadChildren: () => import('./nurse-profile/nurse-profile.module').then( m => m.NurseProfilePageModule)
  },
  {
    path: 'info',
    loadChildren: () => import('./info/info.module').then( m => m.InfoPageModule)
  },
  {
    path: 'info',
    loadChildren: () => import('./info/info.module').then( m => m.InfoPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
