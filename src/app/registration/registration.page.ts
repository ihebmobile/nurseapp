import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormControl, AbstractControl, ValidatorFn } from '@angular/forms';
import { NavController, ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
import { UiService } from '../services/ui.service';
import { MapPage } from '../map/map.page';
import { AuthService } from '../services/auth.service';
import { Events } from '../services/Events';


@Component({
  selector: 'app-registration',
  templateUrl: './registration.page.html',
  styleUrls: ['./registration.page.scss'],
})
export class RegistrationPage implements OnInit {

  loginForm: FormGroup;
  submitAttempt: boolean = false;
  private genre:any=null;
  
  private address:any={
    address:'',
    country:'',
    lat: 35.9022267,
    long: 10.3497895
  }
  specialities: any;
  currentType: any='customer';
  constructor(private auth:AuthService,private events:Events,private ui: UiService, private navCtr: NavController, private formBuilder: FormBuilder, private modalcontroller: ModalController, private router: Router) {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.required],
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      address: ['', Validators.required],
      type: ['', Validators.required],
      description: ['', Validators.required],
      phone: ["",Validators.compose([Validators.required, Validators.minLength(8)])]
    },{ validator: this.isNumber("phone") })
  }

  isNumber(
    controlkey: string
  ) {
    return (group: FormGroup) => {
      let phone = group.controls[controlkey];
      if (isNaN(phone.value)) {
        return phone.setErrors({ notConform: true });
      } else if(phone.value.length<6){
        return phone.setErrors({ minLength: true });
      }else if(phone.value.length > 14){
        return phone.setErrors({ maxLength: true });
      } else{
        return phone.setErrors(null);
      }
    };
  }

   forbiddenNameValidator(nameRe: RegExp): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} | null => {
      const forbidden =  !isNaN(control.value);
      return forbidden ? {forbiddenName: {value: control.value}} : null;
    };
  }
  ngOnInit() {
    this.loadSpecialities();
  }

  login() {
    this.submitAttempt = true
    if (this.loginForm.valid) {
      let credencial = this.loginForm.value;
      if (credencial.email == "customer@gmail.com")
        this.navCtr.navigateForward("/nurses");
      else if (credencial.email == "nurse@gmail.com") {
        let nurse = { icon: "../assets/imgs/logo-login.png", name: "M. Ali Bahri", rate: "3,5", description: "professionnel , Exp 25 ans , 12 km from tunisia", works: { from: '1 AM', to: '6 PM' } };
        this.router.navigate(['/reservations'], {
          state: {
            nurse: JSON.stringify(nurse),
            type: 'customer'
          }
        });
      }
    }
  }

  register() {
    this.submitAttempt = true;
    if (this.loginForm.valid && this.genre!=null) {
      let credencial = this.loginForm.value;
      this.auth.register({...credencial,address:this.address,genre:this.genre}).then((res:any)=>{
        let user=res.user;
        console.log(user);
        this.events.publish('user-login',user.id);
        if(user.type=="customer")
        this.navCtr.navigateForward("/type");
      else if(user.type=="nurse"){
        let nurse=user;
        this.router.navigate(['/reservations'], {
          state: {
          nurse: JSON.stringify(nurse),
          type: 'customer'
          }
      });
      }
    }).catch(err=>{
        console.log(err);
      })
     
    }
  }

  openMap() {
      this.ui.modal({ component: MapPage, componentProps: { address:this.address} }).then(res => {
      this.address=res.data;
      console.log(this.address);
    });
  }

  loadSpecialities(){
    this.auth.loadSpecialities().then((specialities:any)=>{
      this.specialities=specialities;
      console.log(specialities);
    }).catch(err=>console.log(err))
  }

  typeChanged(event){
    setTimeout(() => {
      if(this.currentType=="customer"){
        this.loginForm.removeControl('speciality');
      }else{
        this.loginForm.addControl('speciality',  new FormControl('', Validators.required));
      }
    }, 200);

  }


  goBack() {
    console.log("this.goBack");
    this.navCtr.back();
  }
}
