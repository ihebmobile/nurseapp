import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { NativeGeocoder, NativeGeocoderResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';
import { NavParams, NavController, ModalController } from '@ionic/angular';
import { Plugins } from '@capacitor/core';
import { Router } from '@angular/router';
import { UiService } from '../services/ui.service';

const { Geolocation } = Plugins
declare var google;
@Component({
  selector: 'app-map',
  templateUrl: './map.page.html',
  styleUrls: ['./map.page.scss'],
})
export class MapPage implements OnInit {
  @ViewChild('map') gmap: ElementRef;
  map: any;
  marker: any;
  geocoder: any;
  location: any = {};
  isLoading = false
  mapOptions: any = {
    center: new google.maps.LatLng(36.7948624, 10.0732384),
    zoom: 2,
    disableDefaultUI: true,
    clickableIcons: false
  }
  newLocation: any = {}
  constructor(private navParams:NavParams,private ui:UiService,public viewCtrl: ModalController,private router:Router,private navCtr:NavController) 
  {
    
  }
ionViewDidEnter(){
  this.mapInitializer()
  this.initAutocompleteInput()
  
  this.newLocation = {...this.location}
  if (!this.location.long)
    this.getCurrentPosition();
  else {
    this.mapOptions.center = new google.maps.LatLng(Number(this.location.lat), Number(this.location.long))
    setTimeout(() => {
      this.centerMap(false)
    }, 500);
  }
}
  ngOnInit() {
    if(this.navParams.get("address"))
    this.location=this.navParams.get("address") || {};
  }

  initAutocompleteInput() {
    const locationInput = <HTMLInputElement>document.querySelector('.custom-address-input input');
    const options = {
      types : ['geocode']
    }
    const autocomplete = new google.maps.places.Autocomplete(locationInput, options);
    google.maps.event.addListener(autocomplete, 'place_changed', ()=> {
      const place = autocomplete.getPlace();
      this.newLocation = {
        ...this.newLocation,
        address: place.formatted_address,
        ...this.formatAddressComponents(place.address_components)
      }
      this.mapOptions.center = place.geometry.location
      this.centerMap()
    })
  }

  async mapInitializer() {
    this.map = new google.maps.Map(this.gmap.nativeElement, this.mapOptions);
    this.map.addListener('click', (e)=> {
      this.placeMarkerAndPanTo(e.latLng)
    })
  }

  placeMarkerAndPanTo(latLng) {
    this.mapOptions.center = latLng
    this.centerMap();
  }

  async centerMap(geocode = true) {
    this.map.setZoom(15)
    this.map.panTo(this.mapOptions.center)
    this.addMarker()
    if (geocode) {
      const location = await this.geocodeLatLng(this.mapOptions.center)
      this.newLocation = {
        ...this.newLocation,
        ...location
      }
    }
  }

  addMarker() {
    if (this.marker) {
      this.marker.setPosition(this.mapOptions.center)
    }else {
      this.marker = new google.maps.Marker({
        position: this.mapOptions.center,
        map: this.map,
        icon: 'assets/imgs/marker.png'
      })
    }
  }

  async getCurrentPosition() {
    //  this.ui.loading('geolocalization');
    try {
      const resp = await Geolocation.getCurrentPosition({maximumAge: 10000, timeout: 10000})
      this.mapOptions.center = new google.maps.LatLng(Number(resp.coords.latitude), Number(resp.coords.longitude))
      this.centerMap()
      // this.ui.unLoading();
    }catch(error) {
      this.ui.fireError('Check your location settings or pin a marker on the map')
      console.log('Error getting location', error);
      // this.ui.unLoading();
    }
  }

  geocodeLatLng(latlng): Promise<any> {
    return new Promise((resolve, reject)=> {
      const geocoder = new google.maps.Geocoder();
      this.isLoading = true
      geocoder.geocode({'location': latlng}, (results, status)=> {
        this.isLoading = false
        if (status === 'OK') {
          if (results[1]) {
            const location = {
              address: results[0].formatted_address,
              ...this.formatAddressComponents(results[0].address_components)
            }
            resolve(location)
          } else {
            this.ui.fireError('Please try again')
            reject('No results found')
          }
        } else {
          this.ui.fireError('Please try again')
          reject('Geocoder failed due to: ' + status)
        }
      })
    })
  }

  formatAddressComponents(addressComponents) {
    let country, country_code, city, postal_code
    addressComponents.forEach(address => {
      switch (address.types[0]) {
        case 'locality': city = address.long_name; break;
        case 'country':
          country = address.long_name; 
          country_code = address.short_name;
        break;
        case 'postal_code_prefix': postal_code = address.long_name; break;
      }
    })
    return {
      country,
      country_code,
      city,
      postal_code
    }
  }

  confirmAddress() {
    this.newLocation = {
      ...this.newLocation,
      lat: this.mapOptions.center.lat(),
      long: this.mapOptions.center.lng()
    }
    this.closeModal(this.newLocation);
  }

  closeModal(location?) {
    this.viewCtrl.dismiss(location).then(res=>console.log("dismiss")).catch(err=>console.log(err));
  }

}



