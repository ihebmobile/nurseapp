import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'time'
})
export class TimePipe implements PipeTransform {

  transform(value: any, ...args: unknown[]): unknown {
   
    
      
      let timeTokens =value.split(':');
      let newDate= new Date(1970, 0, 1, timeTokens[0], timeTokens[1], timeTokens[2]);
    
    return newDate;
  }

}
