import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReservationsPageRoutingModule } from './reservations-routing.module';

import { ReservationsPage } from './reservations.page';
import { StarRatingModule } from 'ionic5-star-rating';
import { PipesModule } from '../pipes/pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StarRatingModule,
    ReactiveFormsModule,
    ReservationsPageRoutingModule,
    PipesModule
  ],
  declarations: [ReservationsPage]
})
export class ReservationsPageModule {}
