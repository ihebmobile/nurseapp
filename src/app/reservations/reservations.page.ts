import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController, AlertController } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Events } from '../services/Events';

@Component({
  selector: 'app-reservations',
  templateUrl: './reservations.page.html',
  styleUrls: ['./reservations.page.scss'],
})
export class ReservationsPage implements OnInit {
  private nurse: any = {};
  private showAdd: boolean = false;
  timeForm: FormGroup;
  private submitAttempt: boolean = false;
  private addservice: boolean = false;
  private from: any = {
    hours: null,
    minutes: null,
  }

  private to: any = {
    hours: null,
    minutes: null,
  }
  private days: any = ["LU", "MA", "ME", "JE", "VE", "SA", "DI"];
  private availability: any = {
    "LU": ["11:30 AM", "11:50 AM"],
    "MA": ["11:30 AM", "10:00 AM", "11:50 AM", "6:00 PM"],
    "ME": ["11:30 AM", "11:50 AM"],
    "JE": ["11:00 AM", "11:30 AM", "11:50 AM"],
    "VE": ["11:30 AM", "12:50 AM"],
    "SA": ["11:30 AM", "9:50 AM"],
    "DI": ["11:30 AM", "11:50 AM"]
  };

  private selectedtime: any = null;
  private selectedIndex: any = null;
  private current_day: number;
  serviceForm: FormGroup;
  private current = new Date();
  private weekstart = this.current.getDate() - this.current.getDay() + 1;
  private days_numbers: any[] = [];
  private type: any;
  constructor(private auth: AuthService, private events: Events, private formBuilder: FormBuilder, private confirm: AlertController, private route: ActivatedRoute, private navCtr: NavController, private router: Router) {

    this.serviceForm = this.formBuilder.group({
      name: ['', Validators.required],
      price: ['', Validators.required],
      duration: ['', Validators.required]
    })


    this.timeForm = this.formBuilder.group({
      fromHour: ['', [Validators.required, Validators.min(0), Validators.max(23)]],
      fromMin: ['', [Validators.required, Validators.min(0), Validators.max(59)]],
      toHour: ['', [Validators.required, Validators.min(0), Validators.max(23)]],
      toMin: ['', [Validators.required, Validators.min(0), Validators.max(59)]]
    })

    this.nurse = this.auth.user;
    this.availability = this.nurse.works;
    console.log(this.availability);
    console.log(this.selectedIndex);
    let d = new Date();
    this.current_day = d.getDay();
    console.log(this.current_day);
    console.log(this.weekstart);
    let i = 1;
    this.days.forEach(day => {
      this.days_numbers[i - 1] = this.current.getDate() - this.current.getDay() + i;
      i++;
    })

    this.selectedIndex = this.router.getCurrentNavigation().extras.state.selectedDay || this.current_day - 1;
    console.log(this.days_numbers);
    this.events.subscribe("changedProfile", user => {
      this.nurse = user;
    });
  }

  ngOnInit() {
    setTimeout(() => {
      console.log(this.nurse);
    }, 2000);

  }
  goBack() {
    this.navCtr.pop();
  }

  // checkAvailable(selectedDay){
  //   this.router.navigate(['/reservation-dtails'], {
  //     state: {
  //     nurse: JSON.stringify(this.nurse),
  //     type: 'customer',
  //     selectedDay:selectedDay
  //     }
  // });
  // }

  delete(selectedIndex, selectedTime) {
    this.selectedtime = selectedTime;
    setTimeout(() => {
      console.log(selectedIndex);
      console.log(selectedTime);
      this.showConfirm(selectedIndex, selectedTime)
    }, 200);

  }

  showConfirm(selectedIndex, selectedTime) {
    this.confirm.create({
      header: "Supprimer l'intervalle",
      subHeader: 'confirmer la suppression',
      message: 'Êtes-vous sûr? vous voulez supprimer cet intervalle de temps?',
      buttons: [
        {
          text: 'Oui',
          handler: () => {
            console.log('I care about humanity');
            this.availability[selectedIndex + 1].splice(selectedTime, 1);
          }
        },
        {
          text: 'Non',
          handler: () => {
            console.log('Let me think');
          }
        }
      ]
    }).then(res => {
      res.present();
    });
  }
  add() {
    this.submitAttempt = true;
    if (this.timeForm.valid) {
      let from = this.from.hours + ":" + this.from.minutes;
      let to = this.to.hours + ":" + this.to.minutes;
      // console.log((this.availability[this.days[this.selectedIndex]]).findIndex(element=>{element==time}))
      // if((this.availability[this.days[this.selectedIndex]]).findIndex(element=>{element==time})<0)
      this.availability[this.selectedIndex + 1] = [{ id: "new", from: from, to: to }];
      this.submitAttempt = false;
      setTimeout(() => {
        this.timeForm.reset()
      }, 200);
    }
  }
  clear() {
    this.availability[this.selectedIndex + 1] = [];
  }
  gotoReservations() {
    this.router.navigate(['/reservation-dtails'], {
      state: {
        nurse: JSON.stringify(this.nurse),
        type: 'nurse'
      }
    });
  }

  confirmSchedule() {
    console.log("Scedule");
    this.auth.confirmSceduel(this.availability).then((res: any) => {
      this.availability = res.data.works;
      this.auth.user.works = res.data.works;
      this.auth.storeUser(this.auth.user);

    }).catch(err => console.log(err));
  }


  addService() {
    this.addservice = true;
    if (this.serviceForm.valid) {
      let credencial = this.serviceForm.value;
      this.auth.addService(credencial).then(res => {
        this.nurse.services.push(res);
        this.auth.storeUser(this.nurse);
        this.addservice = false;
        this.serviceForm.reset();
        this.showAdd=false;
      }).catch(err => console.log(err));
    }
  }

  deleteService(service_id, index) {
    this.auth.deleteService(service_id).then(res => {
      this.nurse.services.splice(index, 1);
      this.auth.storeUser(this.nurse);
    }).catch(err => console.log(err));
  }

}


