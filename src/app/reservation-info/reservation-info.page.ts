import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { NativeGeocoder, NativeGeocoderResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';
import {  NavController, ModalController, IonInfiniteScroll } from '@ionic/angular';
import { Plugins } from '@capacitor/core';
import { Router } from '@angular/router';
import { UiService } from '../services/ui.service';
import { AuthService } from '../services/auth.service';

const { Geolocation } = Plugins;
declare var google;

@Component({
  selector: 'app-reservation-info',
  templateUrl: './reservation-info.page.html',
  styleUrls: ['./reservation-info.page.scss'],
})
export class ReservationInfoPage implements OnInit {
  @ViewChild('map') gmap: ElementRef;
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  map: any;
  marker: any;
  geocoder: any;
  location: any = {};
  isLoading = false
  mapOptions: any = {
    center: new google.maps.LatLng(36.7948624, 10.0732384),
    zoom: 2,
    disableDefaultUI: true,
    clickableIcons: false
  }
  newLocation: any = {}
  currentlat: number;
  private bookings:any=[];
  currentlong: number;
  private page=1;
  constructor(private auth:AuthService,private ui:UiService,public viewCtrl: ModalController,private router:Router,private navCtr:NavController) 
  {
    
  }
ionViewDidEnter(){
  // this.mapInitializer()
  // this.newLocation = {...this.location}
  // this.getCurrentPosition();
  

}


startNavigating(lat,lng){
  
  let directionsService = new google.maps.DirectionsService;
  let directionsDisplay = new google.maps.DirectionsRenderer;

  directionsDisplay.setMap(this.map);
  directionsService.route({
      origin: {lat:parseFloat(lat), lng: parseFloat(lng)},
      destination: {lat: parseFloat(lat), lng:parseFloat("44,2")},
      travelMode: google.maps.TravelMode['DRIVING']
  }, (res, status) => {
      console.log(res);
      if(status == google.maps.DirectionsStatus.OK){
          directionsDisplay.setDirections(res);
      } else {
          console.warn(status);
      }

  });

}
  ngOnInit() {
    // if(this.navParams.get("address"))
    // this.location=this.navParams.get("address") || {};
    this.auth.getCustomerReservations(this.page).then((res:any)=>{
      this.bookings=res.data;
    }).catch(err=>console.log(err))
  }
  

  loadData(event) {
    setTimeout(() => {
      console.log('Done');
      event.target.complete();

      // App logic to determine if all data is loaded
      // and disable the infinite scroll
      this.page++;
      this.auth.getCustomerReservations(this.page,true).then((res:any)=>{
        if( this.page==res.meta.last_page)
        this.infiniteScroll.disabled = true;
        else
        this.bookings=this.bookings.concat(res.data);
      }).catch(err=>console.log(err))
    }, 500);
  }

 
  async mapInitializer() {
    this.map = new google.maps.Map(this.gmap.nativeElement, this.mapOptions);
    this.map.addListener('click', (e)=> {
      this.placeMarkerAndPanTo(e.latLng)
    })
  }

  placeMarkerAndPanTo(latLng) {
    this.mapOptions.center = latLng
    this.centerMap();
  }

  async centerMap(geocode = true) {
    this.map.setZoom(15)
    this.map.panTo(this.mapOptions.center)
    this.addMarker()
   
  }

  addMarker() {
    if (this.marker) {
      this.marker.setPosition(this.mapOptions.center)
    }else {
      this.marker = new google.maps.Marker({
        position: this.mapOptions.center,
        map: this.map,
        icon: 'assets/imgs/marker.png'
      })
    }
  }

  async getCurrentPosition() {
    //  this.ui.loading('geolocalization');
    try {
      const resp = await Geolocation.getCurrentPosition({maximumAge: 10000, timeout: 10000})
      this.mapOptions.center = new google.maps.LatLng(Number(resp.coords.latitude), Number(resp.coords.longitude))
      this.centerMap()
      this.startNavigating(resp.coords.latitude,resp.coords.longitude);
      // this.ui.unLoading();
    }catch(error) {
      this.ui.fireError('Check your location settings or pin a marker on the map')
      console.log('Error getting location', error);
      // this.ui.unLoading();
    }
  }
  closeModal(location?) {
    this.viewCtrl.dismiss(location).then(res=>console.log("dismiss")).catch(err=>console.log(err));
  }

  goBack(){
    this.navCtr.pop();
  }
}
