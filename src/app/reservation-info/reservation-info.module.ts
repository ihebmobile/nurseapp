import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReservationInfoPageRoutingModule } from './reservation-info-routing.module';

import { ReservationInfoPage } from './reservation-info.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReservationInfoPageRoutingModule
  ],
  declarations: [ReservationInfoPage]
})
export class ReservationInfoPageModule {}
