import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { UiService } from '../services/ui.service';
import { MapPage } from '../map/map.page';
import { Events } from '../services/Events';

@Component({
  selector: 'app-nurse-profile',
  templateUrl: './nurse-profile.page.html',
  styleUrls: ['./nurse-profile.page.scss'],
})
export class NurseProfilePage implements OnInit {
  nurse: any;

  loginForm: FormGroup;
  submitAttempt: boolean = false;
  photo: any=null;
  constructor(private navCtr: NavController,private events:Events, private ui: UiService, private formBuilder: FormBuilder, private auth: AuthService, private route: ActivatedRoute, private router: Router) {
    this.nurse = (JSON.parse(this.router.getCurrentNavigation().extras.state.nurse));
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      name: ['', Validators.required],
      address: ['', Validators.required],
      description: ['', Validators.required],
      phone: ['', Validators.required]
    })
  }

  ngOnInit() {
  }

  goBack() {
    this.navCtr.pop();
  }

  openMap() {
    if(this.auth.user.type!='customer'){
    this.ui.modal({ component: MapPage, componentProps: { address: this.nurse.location } }).then(res => {
      this.nurse.location = res.data;
      console.log(this.nurse.location);
    });
  }
  }

  register() {
    this.submitAttempt = true;
    if (this.loginForm.valid) {
      let credencial = this.loginForm.value;
      this.auth.updateProfile({ ...credencial, address: this.nurse.location,photo:this.photo}).then((res: any) => {
        let user = res;
        this.nurse=res;
        this.events.publish("changedProfile",res);
        console.log(user);
      }).catch(err => {
        console.log(err);
      })
    }
  }

  uploadIamge(){
    if(this.auth.user.type!='customer'){
    this.ui.imageType().then((res:any)=>{
      this.photo=res;
      this.nurse.icon=res;
      console.log(res);
    }).catch(err=>console.log(err));
  }
}
}
