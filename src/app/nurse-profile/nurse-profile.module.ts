import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NurseProfilePageRoutingModule } from './nurse-profile-routing.module';
import { StarRatingModule } from 'ionic5-star-rating';
import { NurseProfilePage } from './nurse-profile.page';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    StarRatingModule,
    NurseProfilePageRoutingModule
  ],
  declarations: [NurseProfilePage]
})
export class NurseProfilePageModule {}
