import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NurseProfilePage } from './nurse-profile.page';

const routes: Routes = [
  {
    path: '',
    component: NurseProfilePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NurseProfilePageRoutingModule {}
