import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NurseProfilePage } from './nurse-profile.page';

describe('NurseProfilePage', () => {
  let component: NurseProfilePage;
  let fixture: ComponentFixture<NurseProfilePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NurseProfilePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NurseProfilePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
