import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NurseDtailsPage } from './nurse-dtails.page';

describe('NurseDtailsPage', () => {
  let component: NurseDtailsPage;
  let fixture: ComponentFixture<NurseDtailsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NurseDtailsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NurseDtailsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
