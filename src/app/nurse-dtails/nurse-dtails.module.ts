import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NurseDtailsPageRoutingModule } from './nurse-dtails-routing.module';

import { NurseDtailsPage } from './nurse-dtails.page';
import { StarRatingModule } from 'ionic5-star-rating';
import { CalendarModule } from 'ion2-calendar';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NurseDtailsPageRoutingModule,
    StarRatingModule,
    CalendarModule
  ],
  declarations: [NurseDtailsPage]
})
export class NurseDtailsPageModule {}
