import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { CalendarComponentOptions, DayConfig } from 'ion2-calendar';

@Component({
  selector: 'app-nurse-dtails',
  templateUrl: './nurse-dtails.page.html',
  styleUrls: ['./nurse-dtails.page.scss'],
})
export class NurseDtailsPage implements OnInit {
// Calendar Component
dateMulti: string;
selectedSerice:any;
private services:any=[];
// typeCalendar: 'string';
private _daysConfig:DayConfig[] = [];
optionsMulti: CalendarComponentOptions = {
  pickMode: 'single',
  color:'primary',
  weekdays: ['DI','LU', 'MA', 'ME', 'JE', 'VE', 'SA'],
  monthPickerFormat:['JANV', 'FEVR', 'MARS', 'AVRI', 'MAI', 'JUIN', 'JUIL', 'AOUT', 'SEPT', 'OCTO', 'NOVE', 'DESC'],
  daysConfig: this._daysConfig
};
// daysConfig DayConfig[] {
//   cssClass:'',
//   date	:new Date(),
//   marked:true	,
//   disable:true,
//   title	:'Today',
//   subTitle:'Here We Are'
//     },

  // markDisabled = (date: Date) => {
  //       var current = new Date();
  //       date.getDay();
  //       return date < current;
  //   };
  // 
private nurse:any={};
private days:any=["LU","MA","ME","JE","VE","SA","DI"];
private selectedIndex:any=null;
private current_day:number;
private  current = new Date(); 
private weekstart = this.current.getDate() - this.current.getDay() +1; 
private days_numbers:any[]=[];
private type:any;
  constructor(private auth:AuthService,private route: ActivatedRoute,private navCtr:NavController,private router:Router) {
    this.nurse=(JSON.parse(this.router.getCurrentNavigation().extras.state.nurse));
    let d = new Date();
    this.current_day=d.getDay();
    console.log(this.current_day);
    console.log(this.weekstart);
    let i=1;
    this.days.forEach(day=>{
      this.days_numbers[i-1]=this.current.getDate() - this.current.getDay() +i;
      i++;
    })
    console.log(this.days_numbers);
   }

  ngOnInit() {
    this.auth.getNurse(this.nurse.id).then(res=>{
      this.nurse=res;
    }).catch(err=>console.log(err));
    // setTimeout(() => {
    //   console.log(this.nurse);
    // }, 2000);
    // this.auth.getservices().then((res:any)=>{
    //   this.services=res;
    // }).catch(err=>console.log(err))
    
  }
  goBack(){
    this.navCtr.pop();
  }

  checkAvailable(selectedDay){
    console.log(selectedDay);
    let currentdate=new Date(this.dateMulti);
    let day=(currentdate.getDay() ==0 ? 7 : currentdate.getDay()) || this.current_day;
    console.log(day);
    this.auth.checkAvailability(day,this.dateMulti,this.nurse.id,this.selectedSerice).then((res:any)=>{
      console.log(res);
      this.router.navigate(['/join-reservation'], {
        state: {
        nurse: JSON.stringify(this.nurse),
        type: 'customer',
        selectedDay:selectedDay,
        availables:res,
        selected_date:this.dateMulti,
        service_id:this.selectedSerice
        }
    });
    }).catch(err=>console.log(err));
  }
  onChange($event){

    // console.log($event);
    console.log(this.dateMulti);
  }


  showProfile(){
    this.router.navigate(['/nurse-profile'], {
      state: {
      nurse: JSON.stringify(this.nurse),
      type: 'customer'
      }
  });
  }
}
