import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NurseDtailsPage } from './nurse-dtails.page';

const routes: Routes = [
  {
    path: '',
    component: NurseDtailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NurseDtailsPageRoutingModule {}
