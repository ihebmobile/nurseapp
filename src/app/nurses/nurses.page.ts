import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { NavigationExtras, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { Plugins } from '@capacitor/core';
import { UiService } from '../services/ui.service';
const { Geolocation } = Plugins;

@Component({
  selector: 'app-nurses',
  templateUrl: './nurses.page.html',
  styleUrls: ['./nurses.page.scss'],
})
export class NursesPage implements OnInit {
  private nureses: any[];
  speciality: any;
  genre: any;
  constructor(private ui:UiService,private auth: AuthService, private navCtr: NavController, private router: Router) {
    this.speciality = (JSON.parse(this.router.getCurrentNavigation().extras.state.speciality));
    this.genre = this.router.getCurrentNavigation().extras.state.type;
  }

  ngOnInit() {
    this.auth.getbestNurse(this.speciality.id);
    // this.auth.getNurses(this.speciality.id,this.genre).then((res: any) => {
    //   console.log(res);
    //   this.nureses = res;
    // }).catch(err => console.log(err));
    this.getCurrentPosition();
  }
  goBack() {

    this.navCtr.pop();
  }

  async getCurrentPosition() {
    //  this.ui.loading('geolocalization');
    try {
      const resp = await Geolocation.getCurrentPosition({enableHighAccuracy: true,maximumAge: 10000, timeout: 10000})
      this.auth.getByPos(this.speciality.id,this.genre,resp.coords.latitude,resp.coords.longitude).then((res: any) => {
        console.log(res);
        this.nureses = res;
      }).catch(err => console.log(err));
      // resp.coords.latitude,resp.coords.longitude
      // this.ui.unLoading();
    }catch(error) {
      this.ui.fireError('Verifier vos parametre de geolocalisation')
      console.log('Error getting location', error);
      // this.ui.unLoading();
    }
  }
  goDtails(nurse: any) {
    this.router.navigate(['/nurse-dtails'], {
      state: {
        nurse: JSON.stringify(nurse),
        type: 'customer'
      }
    });
  }

  goLists() {
    this.navCtr.navigateForward("reservation-info");
  }
}
