import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NursesPageRoutingModule } from './nurses-routing.module';

import { NursesPage } from './nurses.page';
import { StarRatingModule } from 'ionic5-star-rating';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NursesPageRoutingModule,
    StarRatingModule
  ],
  declarations: [NursesPage]
})
export class NursesPageModule {}
