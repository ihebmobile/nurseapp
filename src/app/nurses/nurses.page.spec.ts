import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NursesPage } from './nurses.page';

describe('NursesPage', () => {
  let component: NursesPage;
  let fixture: ComponentFixture<NursesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NursesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NursesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
