import { Injectable } from '@angular/core';
import { AppConfigService } from './app-config.service';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  appPrefix: string = this.appConfig.appPrefixes.app;
  constructor(
    private storage: Storage, 
    private appConfig: AppConfigService
  ){}

  public set(key: string, value: any) {
    return this.storage.set(`${this.appPrefix}-${key}`,value);
  }
  public async get(key) {
    return await this.storage.get(`${this.appPrefix}-${key}`);
  }
  public async remove(key) {
    return await this.storage.remove(`${this.appPrefix}-${key}`);
  }
  public clear() {
    this.storage.clear().then(() => {
      console.log('all keys cleared');
    });
  }
}
