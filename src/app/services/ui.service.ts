import { Injectable } from '@angular/core';
import { AlertController, ActionSheetController, ModalController, LoadingController, ToastController } from '@ionic/angular';
import { CameraService } from './camera.service';
import { LoadingOptions, ToastOptions, ModalOptions, AlertOptions, ToastButton  } from '@ionic/core';

@Injectable({
  providedIn: 'root'
})
export class UiService {
  currentLoading: HTMLIonLoadingElement
  toastInst: any
  constructor(
    private alertCtrl: AlertController,
    private actionCtrl: ActionSheetController,
    private modalCtrl: ModalController,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private camera: CameraService,
  ){ }

  alert(options: AlertOptions) {
    return new Promise(async (resolve, reject) => {
      const alert = await this.alertCtrl.create({
        ...options,
        buttons: [
          {
            text: "D'accord",
            handler: () => {
              resolve(true);
            }
          }
        ]
      });
      alert.present();
    });
  }

  imageType(withCrop = false): Promise<string> {
    return new Promise(async (resolve) => {
      const action = await this.actionCtrl.create({
        header: 'Ajouter un image',
        buttons: [
          {
            text: 'Camera',
            icon: 'camera',
            handler: async () => {
              const base64 = await this.camera.fromCamera()
              if (withCrop) {
                const croppedImage: any = await this.modal({
                  component: 'CropperPage',
                  componentProps: {image: base64},
                  cssClass: 'custom-modal'
                })
                const image = croppedImage || base64
                resolve(image)
              }else {
                resolve(base64)
              }
            }
          }, {
            text: "Galerie",
            icon: 'images',
            handler: async () => {
              const base64 = await this.camera.fromGalley()
              console.log(base64);
              if (withCrop) {
                const croppedImage: any = await this.modal({
                  component: 'CropperPage',
                  componentProps: {image: base64},
                  cssClass: 'custom-modal'
                })
                const image = croppedImage || base64
                resolve(image)
              }else{
                resolve(base64)
              }
            }
          }, {
            text: "Annuler",
            icon: 'close',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          }
        ]
      });
      action.present();
    })
  }

  async loading(message: string = null, duration: number = null) {
    // Dismiss previously created loading
    if (this.currentLoading != null) {
      this.unLoading();
    }

    this.currentLoading = await this.loadingCtrl.create({
      duration: duration,
      message: message
    });

    return await this.currentLoading.present();
  }

  async unLoading() {
    if (this.currentLoading != null) {
      await this.currentLoading.dismiss();
      // this.currentLoading = null;
    }
    return;
  }

  async toast(options: ToastOptions) {
    // let duration = options.duration >= 0 ? options.duration : 2000;
    if (this.toastInst) {
      this.toastInst.dismiss();
    }
    const buttons: ToastButton[] = !options.duration ? [
      {
        icon: 'close',
        role: 'cancel'
      }
    ] : null
    this.toastInst = await this.toastCtrl.create({
      ...options,
      cssClass: options.cssClass || '',
      position: options.position || 'bottom',
      buttons
    });
    this.toastInst.present();
  }

  dismissToast() {
    if (this.toastInst)
      return this.toastInst.dismiss();
  }

  modal(options?: ModalOptions): Promise<any> {
    return new Promise(async (resolve)=> {
      const modalInst = await this.modalCtrl.create(options);
      modalInst.onDidDismiss().then(data => resolve(data))
      await modalInst.present();
    })
  }

  fireError(err, duration = 2500) {
    this.unLoading();
    let messages = '';
    if (typeof (err) === 'string') {
      messages = err;
    } else {
      if (err.status == 500 || err.status == 0) {
        messages = 'Erreur Interne du Serveur'
      } else {
        const serverResp = err.error
        for (let fieldName in serverResp.errors) {
          if (fieldName == 'customError') {
            messages = serverResp.errors[fieldName]
          } else {
            for (let e of serverResp.errors[fieldName]) {
              messages = messages + e + '\n'
            }
          }
        }
      }
    }
    this.toast({ message: messages, cssClass: 'toast-error', color: 'danger', duration });
  }

  fireSuccess(message: string, duration = 2500) {
    this.toast({ message: message, cssClass: 'toast-success', color: 'success', duration });
  }

  confirmation(header, subHeader,message,yesBtn?,noBtn?) {
    return new Promise(async (resolve,reject)=> {
    this.alertCtrl.create({
      header: header,
      subHeader:subHeader,
      message: message,
      buttons: [
        {
          text: yesBtn || 'Oui',
          handler: () => {
            console.log('I care about humanity');
            resolve(true)
          }
        },
        {
          text: noBtn || 'Non',
          handler: () => {
            console.log('Let me think');
            reject(false)
          }
        }
      ]
    }).then(res => {
      res.present();
    });
  })
  }
}
