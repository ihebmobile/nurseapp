import { Injectable, NgZone } from '@angular/core';
import { UiService } from './ui.service';
import { StorageService } from './storage.service';
import { AppConfigService } from './app-config.service';
import { HttpHelperService } from './http-helper.service';
import { Events } from './Events';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  updateProfile(user: any) {
    return new Promise(async (resolve, reject) => {
      this.ui.loading();
      let data = {
        ...user
      }
      this.httpHelper.request('post', `/profile/edit`, data).subscribe((response) => {
        this.ui.unLoading();
        let resp = response;
        this.storeUser(resp.user);
        this.ui.fireSuccess('Données modifiées avec succès');
        resolve(resp.user);
      }, err => {
        this.ui.fireError(err)
        reject(err)
      })
    })
  }
  deleteService(service_id: any) {
    return new Promise(async (resolve, reject) => {
      this.ui.loading();
      this.httpHelper.request('post', `/service/delete`,{service_id}).subscribe((response) => {
        this.ui.unLoading();
        let resp = response;
        resolve(resp);
      }, err => {
        this.ui.fireError(err)
        this.ui.unLoading();
        reject(err)
      })
    })
  }

  addService(credencial: any) {
    return new Promise(async (resolve, reject) => {
      this.ui.loading();
      this.httpHelper.request('post', `/service/add`,{...credencial}).subscribe((response) => {
        this.ui.unLoading();
        let resp = response;
        resolve(resp);
      }, err => {
        this.ui.fireError(err)
        this.ui.unLoading();
        reject(err)
      })
    })
  }
  getNurse(id: any) {
    return new Promise(async (resolve, reject) => {
      this.ui.loading();
      this.httpHelper.request('get', `/nurse/${id}`).subscribe((response) => {
        this.ui.unLoading();
        let resp = response.data;
        resolve(resp);
      }, err => {
        this.ui.fireError(err)
        this.ui.unLoading();
        reject(err)
      })
    })
  }

  public user: any = null;
  public bestNurse: any = null;
  public role = null;
  public isAuthenticated = false;
  public authToken: string;
  public API = this.appConfig.API;
  public BaseAPI = this.appConfig.BASE_API;
  deviceId: string
  deviceIdPrefix = this.appConfig.appPrefixes.deviceId
  currentUserPrefix = this.appConfig.appPrefixes.user
  tokenPrefix = this.appConfig.appPrefixes.token
  constructor(
    private httpHelper: HttpHelperService,
    private appConfig: AppConfigService,
    public ui: UiService,
    private storage: StorageService,
    private events: Events,
    private zone: NgZone,
  ) {


  }

    resetPassword(credential) {
    return new Promise(async (resolve, reject) => {
      this.ui.loading();
      this.httpHelper.request('post', `/reset`, {...credential}).subscribe((response) => {
        let resp = response;
        this.ui.fireSuccess('Mot de passe rénitialiser avec succeé');
        this.ui.unLoading();
        resolve(resp);
      }, err => {
        this.ui.fireError(err)
        this.ui.unLoading();
        reject(err)
      })
    })
  }




  joinReservation(selected_date: any, selected_time: any, service_id: any) {
    return new Promise(async (resolve, reject) => {
      this.ui.loading();
      this.httpHelper.request('post', `/join/reservation`, { selected_date, selected_time, service_id }).subscribe((response) => {
        this.ui.unLoading();
        let resp = response;
        this.ui.fireSuccess('La réservation a réussi')
        resolve(resp);
      }, err => {
        this.ui.fireError(err)
        this.ui.unLoading();
        reject(err)
      })
    })
  }

  getbestNurse(speciality_id) {
    return new Promise(async (resolve, reject) => {
      this.httpHelper.request('get', `/best/nurse/${speciality_id}`).subscribe((response) => {
        let resp = response.data;
        this.bestNurse = resp;
        resolve(resp);
      }, err => {
        reject(err)
      })
    })
  }



  confirmReservation(id: any) {
    return new Promise(async (resolve, reject) => {
      this.ui.loading();
      this.httpHelper.request('get', `/confirm/reservation/${id}`).subscribe((response) => {
        this.ui.unLoading();
        let resp = response;
        resolve(resp.data);
      }, err => {
        this.ui.fireError(err)
        this.ui.unLoading();
        reject(err)
      })
    });
  }

  forgotPassword(credential: any) {
    return new Promise(async (resolve, reject) => {
      this.ui.loading();
      this.httpHelper.request('post', `/forgot`, { ...credential }).subscribe((response) => {
        this.ui.unLoading();
        let resp = response;
        resolve(resp.data);
      }, err => {
        this.ui.fireError(err)
        this.ui.unLoading();
        reject(err)
      })
    })
  }

  getCustomerReservations(page,noloader?) {
    return new Promise(async (resolve, reject) => {
      if(!noloader)
      this.ui.loading();
      this.httpHelper.request('get', `/customer/reservations?page=${page}`).subscribe((response) => {
        if(!noloader)
        this.ui.unLoading();
        let resp = response;
        resolve(resp);
      }, err => {
        this.ui.fireError(err)
        if(!noloader)
        this.ui.unLoading();
        reject(err)
      })
    })
  }
  getReservations() {
    return new Promise(async (resolve, reject) => {
      this.ui.loading();
      this.httpHelper.request('get', `/reservations`).subscribe((response) => {
        this.ui.unLoading();
        let resp = response;
        resolve(resp);
      }, err => {
        this.ui.fireError(err)
        this.ui.unLoading();
        reject(err)
      })
    })
  }


  confirmSceduel(schedual) {
    return new Promise(async (resolve, reject) => {
      this.ui.loading();
      this.httpHelper.request('post', `/confirmScheduel`, { schedual }).subscribe((response) => {
        this.ui.unLoading();
        let resp = response;
        this.ui.fireSuccess('Votre calendrier a été modifié')
        resolve(resp);
      }, err => {
        this.ui.fireError(err)
        this.ui.unLoading();
        reject(err)
      })
    })
  }


  getservices() {
    return new Promise(async (resolve, reject) => {
      this.ui.loading();
      this.httpHelper.request('get', `/services`).subscribe((response) => {
        this.ui.unLoading();
        let resp = response;
        resolve(resp);
      }, err => {
        this.ui.fireError(err)
        this.ui.unLoading();
        reject(err)
      })
    })
  }

  checkAvailability(selected_day, date, provider_id, service_id) {
    return new Promise(async (resolve, reject) => {
      this.ui.loading();
      this.httpHelper.request('post', `/avaialability`, { selected_day, provider_id, date, service_id }).subscribe((response) => {
        this.ui.unLoading();
        let resp = response;
        // this.ui.fireSuccess('Votre calendrier a été modifié')
        resolve(resp);
      }, err => {
        this.ui.fireError(err)
        this.ui.unLoading();
        reject(err)
      })
    })
  }


  getNurses(speciality_id, genre) {
    return new Promise(async (resolve, reject) => {
      this.ui.loading();
      this.httpHelper.request('get', `/nurses/${speciality_id}/${genre}`).subscribe((response) => {
        this.ui.unLoading();
        let resp = response.data;
        resolve(resp);
      }, err => {
        this.ui.fireError(err)
        this.ui.unLoading();
        reject(err)
      })
    })
  }

  getByPos(speciality_id, genre,lat,long) {
    return new Promise(async (resolve, reject) => {
      this.ui.loading();
      this.httpHelper.request('post', `/nurses/position`,{speciality_id,genre,lat,long}).subscribe((response) => {
        this.ui.unLoading();
        let resp = response.data;
        resolve(resp);
      }, err => {
        this.ui.fireError(err)
        this.ui.unLoading();
        reject(err)
      })
    })
  }
  loadSpecialities() {
    return new Promise(async (resolve, reject) => {
      this.ui.loading();
      this.httpHelper.request('get', `/specialities`).subscribe((response) => {
        this.ui.unLoading();
        let resp = response;
        resolve(resp);
      }, err => {
        this.ui.fireError(err)
        this.ui.unLoading();
        reject(err)
      })
    })
  }
  public loadUserCredentials() {
    return new Promise(async (resolve) => {
      let token = await this.storage.get(this.tokenPrefix);
      let currentUser = await this.storage.get(this.currentUserPrefix);
      if (token && currentUser) {
        this.useCredentials(token, currentUser);
        resolve(currentUser)
      } else {
        resolve()
      }
    })
  }

  storeUserCredentials(token, currentUser) {
    //if (user.remember){
    this.storage.set(this.tokenPrefix, token);
    this.storage.set(this.currentUserPrefix, currentUser);
    //}
    this.useCredentials(token, currentUser);
  }

  storeUser(currentUser) {
    this.storage.set(this.currentUserPrefix, currentUser);
    this.user = currentUser;
  }

  async getUser() {
    if (this.user)
      return this.user;
    else {
      return await this.storage.get(this.currentUserPrefix);
    }
  }

  setUser(user) {
    this.user = user;
    this.storage.set(this.currentUserPrefix, user);
  }

  useCredentials(token, currentUser) {
    console.log('i use the creadentials');
    this.zone.run(() => {
      this.isAuthenticated = true;
      console.log(this.isAuthenticated);
    })

    this.authToken = token;
    this.user = currentUser;
    this.role = currentUser.role;
    console.log(this.user);
  }

  destroyUserCredentials() {
    return new Promise((resolve) => {
      this.authToken = undefined;
      this.isAuthenticated = false;
      this.storage.remove(this.currentUserPrefix);
      this.storage.remove(this.tokenPrefix);
      // this.storage.remove('userSettings');
      this.user = null;
      resolve(true);
    });
  }

  // async getDeviceInfo() {
  //   let deviceId = this.deviceId || await this.storage.get(this.deviceIdPrefix)
  //   const deviceInfo: DeviceInfo = await Device.getInfo()
  //   return {
  //     platform: deviceInfo.platform,
  //     model: deviceInfo.model,
  //     version: deviceInfo.osVersion,
  //     manufacturer: deviceInfo.manufacturer,
  //     deviceId
  //   }
  // }

  // registerDevice(deviceId) {
  //   return new Promise( async (resolve)=> {
  //     let data = {
  //       deviceId
  //     }
  //     this.httpHelper.request('post','/devices', data).subscribe();
  //   })
  // }

  register(user): Promise<any> {
    return new Promise(async (resolve, reject) => {
      this.ui.loading();
      // let deviceId = await this.storage.get('deviceId');
      // let settings = this.appConfig.userSettings.country ? this.appConfig.userSettings : await this.storage.get('userSettings');
      let data = {
        ...user,
        // type,
        // // ...await this.getDeviceInfo(),
        // // settings
      }
      this.httpHelper.request('post', `/register`, data).subscribe((response) => {
        this.ui.unLoading();
        let resp = response;
        this.storeUserCredentials(resp.token, resp.user);
        resolve(resp);
      }, err => {
        this.ui.fireError(err)
        reject(err)
      })
    })
  }

  login(user) {
    return new Promise(async (resolve, reject) => {
      await this.ui.loading();
      let data = {
        ...user,
      }
      this.httpHelper.request('post', '/login', data).subscribe(response => {
        this.ui.unLoading();
        let resp = response;
        this.storeUserCredentials(resp.token, resp.user);
        this.ui.fireSuccess('Bienvenu');
        resolve(resp);
      }, err => {
        this.ui.fireError(err)
        reject(err)
      })
    })
  }

  logout() {
    this.events.publish('user-login', this.user.id);
  }

  // updateProfile(user, type): Promise<any>{
  //   return new Promise( async (resolve, reject)=> {
  //     let settings = this.appConfig.userSettings.country ? this.appConfig.userSettings : await this.storage.get('userSettings');
  //     let data = {
  //       ...user,
  //       ...await this.getDeviceInfo(),
  //       settings
  //     }
  //     this.ui.loading()
  //     this.httpHelper.request('post', `/register/${type}`, data).subscribe((response) => {
  //       this.ui.unLoading();
  //       const user = response;
  //       this.storeUser(user);
  //       resolve(user);
  //     }, err => {
  //       this.ui.fireError(err)
  //       reject(err)
  //     })
  //   })
  // }

  // forgotRequest(email) {
  //   return new Promise((resolve)=> {
  //     this.ui.loading();
  //     this.httpHelper.request('post','/forgot',{email}).subscribe(res => {
  //       this.ui.unLoading();
  //       resolve(res);
  //     }, err => this.ui.fireError(err))
  //   })
  // }

  // resetPassword(password, token) {
  //   return new Promise(async (resolve) => {
  //     const data = {
  //       password,
  //       'password_confirmation': password,
  //       ...await this.getDeviceInfo(),
  //       token
  //     }
  //     this.ui.loading();
  //     return this.httpHelper.request('post','/reset', data).subscribe(res => {
  //       this.ui.unLoading();
  //       let resp = res;
  //       this.storeUserCredentials(resp.token,resp.user);
  //       this.ui.fireSuccess('login.success');
  //       resolve(resp);
  //     }, err => this.ui.fireError(err));
  //   })
  // }


}
