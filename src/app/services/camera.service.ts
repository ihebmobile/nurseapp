import { Injectable } from '@angular/core';
import { Plugins, CameraResultType, CameraOptions, CameraSource, CameraPhoto, Capacitor } from '@capacitor/core';

const { Camera } = Plugins;
@Injectable({
  providedIn: 'root'
})
export class CameraService {
  private fileInput: HTMLInputElement
	constructor() {}	

	private fileToBase64(file): Promise<any> {
		return new Promise((resolve, reject) => {
			const reader = new FileReader();
			reader.readAsDataURL(file);
			reader.onload = (e: any) => resolve(e.target.result)
			reader.onerror = error => reject(error);
		});
	}

	private openFileDialogBox(): Promise<string> {
		return new Promise((resolve, reject)=> {
			if (!this.fileInput) {
				this.fileInput = document.createElement('input')
				this.fileInput.style.position = 'absolute'
				this.fileInput.type = 'file'
				this.fileInput.style.display = 'none'
				document.body.appendChild(this.fileInput)
			}
			this.fileInput.onchange = async (e)=> {
				if (this.fileInput.files.length) {
					try {
						const result = await this.fileToBase64(this.fileInput.files[0])
						if (result instanceof Error)
							reject(result)
						else
							resolve(result)
					}catch (e) {reject(e)}
				}
			}
			setTimeout(() => {
				this.fileInput.click()
			}, 500);
		})
	}

	fromCamera(): Promise<string> {
		return new Promise((resolve) => {
			let options: CameraOptions = {
				allowEditing: true,
				resultType: CameraResultType.Base64,
				quality: 50,
				source: CameraSource.Camera,
				correctOrientation: true
			};
			Camera.getPhoto(options).then((photo: CameraPhoto) => {
				const base64Image = `data:image/${photo.format};base64,${photo.base64String}`;
				resolve(base64Image);
			}, (err) => {
				console.log('err get image : ', err);
			});
		});
	}

	fromGalley(): Promise<string> {
		return new Promise(async (resolve) => {
			if (Capacitor.platform != 'web') {
				let options: CameraOptions = {
					// allowEditing: true,
					resultType: CameraResultType.Base64,
					quality: 50,
					source: CameraSource.Photos,
					correctOrientation: true
				};
				Camera.getPhoto(options).then((photo: CameraPhoto) => {
					const base64Image = `data:image/${photo.format};base64,${photo.base64String}`;
					resolve(base64Image);
				}, (err) => {
					console.log('err get image : ', err);
				});
			}else {
				try {
					const base64Image = await this.openFileDialogBox();
					resolve(base64Image)
				}catch(e) {
					console.log('err get image : ', e);
				}
			}
		});
	}
}
