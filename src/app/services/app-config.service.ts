import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AppConfigService {
  public appPrefixes = {
    app: 'nurse',
    settings: 'userSettings',
    user: 'currentUser',
    init: 'appInit',
    token: 'token',
    deviceId: 'deviceId'
  };
  private localUrl = '192.168.43.133/www/nurse-api/public';
  private prodUrl = 'vps779886.ovh.net:8080';
  public currentUrl = this.prodUrl
  private CONFIG: any = {
    apiUrl: `http://${this.currentUrl}/api`,
    baseAPI: `http://${this.currentUrl}`,
  }

  public API = this.CONFIG.apiUrl;
  public BASE_API = this.CONFIG.baseAPI;
  public CHAT_URL = this.CONFIG.chatUrl;
  public DEV_ENV : boolean = true;
  public ENV: string = 'dev';
  public COUNTRIES: any = [];
  public currentLang;
  public appDir = 'ltr';

  public appLangs = ['fr', 'ar'];
 
  constructor() {}

  public getTwoCharLang() {
    return !navigator.language ? null : this.appLangs.indexOf(navigator.language.substr(0,2)) > -1 ? navigator.language.substr(0,2) : null;
  }

}