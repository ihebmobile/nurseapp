import { Component, OnInit, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { Events } from '../services/Events';
import { UiService } from '../services/ui.service';

@Component({
  selector: 'app-reservation-dtails',
  templateUrl: './reservation-dtails.page.html',
  styleUrls: ['./reservation-dtails.page.scss'],
})
export class ReservationDtailsPage implements OnInit {
  private nurse: any = {};
  private bookings: any[] = [];
  constructor(private ui:UiService,private zone :NgZone,private events: Events, private auth: AuthService, private navCtr: NavController, private router: Router) {
    this.nurse = (JSON.parse(this.router.getCurrentNavigation().extras.state.nurse));
    this.events.subscribe('recieved-reservation', data => {
      console.log("Recieved DATA",data);
      this.zone.run(()=>{
        this.bookings.unshift(data);
        console.log(this.bookings);
      })
      
    })
  }

  ngOnInit() {
    this.auth.getReservations().then((res: any) => {
      this.bookings = res.data;
    }).catch(err => console.log(err))
  }
  goBack() {
    console.log("this.goBack");
    this.navCtr.back();
  }

  goDtails(booking: any) {
    //   this.router.navigate(['/reservation-info'], {
    //     state: {
    //     booking: JSON.stringify(booking),
    //     type: 'nurse'
    //     }
    // });
  }

  changed(booking){
     this.ui.confirmation("Confirmation","Acceptation de réservation" ,"Annulation aprés la confirmation est non authoriser ! ").then((res)=>{
      this.auth.confirmReservation(booking.id).then(res=>{
        booking.confirmed=true;
      }).catch(err=>{
        console.log(err);
      })
     }).catch(res=>{
      booking.confirmed=false;
     })
  }

  FocusState(event){
    // this.ui.confirmation("Confirmation","Vous confirmer cette réservation ?" ,"Annulation aprés la confirmation est non authoriser ! ").then((res)=>{

    // })
  }
}
