import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReservationDtailsPage } from './reservation-dtails.page';

const routes: Routes = [
  {
    path: '',
    component: ReservationDtailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReservationDtailsPageRoutingModule {}
