import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ReservationDtailsPage } from './reservation-dtails.page';

describe('ReservationDtailsPage', () => {
  let component: ReservationDtailsPage;
  let fixture: ComponentFixture<ReservationDtailsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReservationDtailsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ReservationDtailsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
