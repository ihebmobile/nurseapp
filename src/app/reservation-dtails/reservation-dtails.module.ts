import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReservationDtailsPageRoutingModule } from './reservation-dtails-routing.module';

import { ReservationDtailsPage } from './reservation-dtails.page';
import { StarRatingModule } from 'ionic5-star-rating';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StarRatingModule,
    ReservationDtailsPageRoutingModule
  ],
  declarations: [ReservationDtailsPage]
})
export class ReservationDtailsPageModule {}
