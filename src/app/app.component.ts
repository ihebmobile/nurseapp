import { Component } from '@angular/core';

import { Platform, NavController, MenuController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthService } from './services/auth.service';
import { Router } from '@angular/router';
import { OneSignal } from '@ionic-native/onesignal/ngx';

import { Capacitor } from '@capacitor/core';
import { Events } from './services/Events';
import { Plugins } from '@capacitor/core';
import { BranchInitEvent } from 'capacitor-branch-deep-links';

const { BranchDeepLinks } = Plugins;


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private auth: AuthService,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private events: Events,
    private navCtr: NavController,
    private oneSignal: OneSignal,
    private menu:MenuController
  ) {

    this.platform.backButton.subscribeWithPriority(0, () => {
      console.log("back tapped");
    });


    auth.loadUserCredentials().then(() => {
      this.initializeApp();
      console.log("end Loaded");
      let user = this.auth.user;
      if (user?.type == "customer")
        this.navCtr.navigateRoot("/type");
      else if (user?.type == "nurse") {
        let nurse = user;
        this.navCtr.setDirection('root');
        this.router.navigateByUrl('/reservations', {
          state: {
            nurse: JSON.stringify(nurse),
            type: 'customer'
          }
        });
      }
    }).catch(err => {
      this.navCtr.navigateRoot("/home");
      console.log(err);
    })
    this.events.subscribe('user-login', (user_id) => {
      this.setOneSignalExternalId(user_id);
    })

    this.events.subscribe('user-logout', (user_id) => {
      this.logout();
    })
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      if (Capacitor.platform != 'web') {
        this.branchInit();
        this.oneSignalInit();

      }
    });
  }
  async oneSignalInit() {

    this.oneSignal.startInit('edf67a38-ac74-4ae4-88bf-3057c2122a94', '340398941625');

    this.oneSignal.enableSound(true);
    this.oneSignal.enableVibrate(true);
    this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);
    //  this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.None);
    if (this.auth.isAuthenticated && this.auth.user) {
      this.setOneSignalExternalId(this.auth.user.id);
    }

    this.oneSignal.handleNotificationReceived().subscribe((res) => {
      // do something when notification is received
      console.log("This The REsult From One SIgnal :",res);
      if(res.payload?.additionalData?.type=="newRes"){
        console.log(res.payload?.additionalData?.attachment);
       this.events.publish('recieved-reservation',res.payload?.additionalData?.attachment);
      }
    });

    this.oneSignal.handleNotificationOpened().subscribe((res) => {
      // do something when a notification is opened
      console.log("This The REsult From CLicked One SIgnal :",res);
        if(res.notification.payload?.additionalData?.type=="newRes"){
        this.gotoReservations(this.auth.user);
      }
    });

    this.oneSignal.endInit();
  }

  setOneSignalExternalId(user_id) {
    window["plugins"].OneSignal.setExternalUserId(user_id);
  }

  branchInit() {
   console.log(BranchDeepLinks);
    BranchDeepLinks.addListener('init', (event: BranchInitEvent) => {
      console.log(event.referringParams);
      if (event.referringParams["+clicked_branch_link"]) {
        switch (event.referringParams['$deeplink_path']) {
          case 'reset-password':
            this.router.navigate(['/reset-password'], {
              state: {
                token: event.referringParams.token
              }
            });
            break;
        }
      } else { }
    });

    BranchDeepLinks.addListener('initError', (error: any) => {
      console.log(error);
    });
  }

  logout(){
    console.log("logout");
    this.auth.destroyUserCredentials().then((res)=>{
      this.oneSignal.removeExternalUserId();
      this.oneSignal.endInit();
      this.menu.enable(false, 'custom');
      this.menu.close('custom');
      this.navCtr.navigateRoot("/home");
    });
  }
  gotoReservations(nurse){
    this.router.navigate(['/reservation-dtails'], {
      state: {
      nurse: JSON.stringify(nurse),
      type: 'nurse'
      }
  });
  }
  
  openMenue() {
    console.log("tapped")
    this.menu.enable(true, 'custom');
    this.menu.toggle('custom');
  }


  resevations(){
    console.log(this.auth.user.type);
    if(this.auth.user.type=="customer"){
     console.log(this.auth.user.type);
    this.navCtr.navigateForward("reservation-info");
  }else if(this.auth.user.type=="nurse"){
    console.log(this.auth.user.type);
    this.router.navigate(['/reservation-dtails'], {
      state: {
      nurse: JSON.stringify(this.auth.user),
      type: 'customer'
      }
  });
  }
  }

  profile(){
    this.router.navigate(['/nurse-profile'], {
      state: {
        nurse: JSON.stringify(this.auth.user),
        type: 'customer'
      }
    });
  }

  info(){
    this.router.navigate(['/info']);
  }

}
