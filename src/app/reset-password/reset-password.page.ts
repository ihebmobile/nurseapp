import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, AbstractControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.page.html',
  styleUrls: ['./reset-password.page.scss'],
})
export class ResetPasswordPage implements OnInit {
  resetForm: any;
  submitAttempt: boolean = false;
  private token:any=null;
  constructor(private navCtr:NavController,private formBuilder: FormBuilder,private router:Router,private auth:AuthService) {
    this.resetForm =formBuilder.group(
      {
        password: [
          "",
          Validators.compose([Validators.required, Validators.minLength(8)])
        ],
        v_password: [
          "",
          Validators.compose([Validators.required, Validators.minLength(8)])
        ]}, { validator: this.checkIfMatchingPasswords("password", "v_password") });


    this.token = this.router.getCurrentNavigation().extras.state.token;
   }

   checkIfMatchingPasswords(
    passwordKey: string,
    passwordConfirmationKey: string
  ) {
    return (group: FormGroup) => {
      let passwordInput = group.controls[passwordKey],
        passwordConfirmationInput = group.controls[passwordConfirmationKey];
      if (passwordInput.value !== passwordConfirmationInput.value) {
        return passwordConfirmationInput.setErrors({ notEquivalent: true });
      } else {
        return passwordConfirmationInput.setErrors(null);
      }
    };
  }
  ngOnInit() {
   
  }

  changerMotdePasse(){
    this.submitAttempt = true
    if (this.resetForm.valid) {
      let credencial:any = {...this.resetForm.value ,token:this.token};
      this.auth.resetPassword(credencial).then(res=>{
            this.navCtr.navigateRoot('/home');
           }).catch(err=>console.log(err));
    }
  }

}
