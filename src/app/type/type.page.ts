import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-type',
  templateUrl: './type.page.html',
  styleUrls: ['./type.page.scss'],
})
export class TypePage implements OnInit {
specialities:any;
private showType:boolean=false
  speciality: any;
  constructor(private auth:AuthService,private router:Router) { }

  ngOnInit() {

    console.log(this.auth.isAuthenticated);
    this.auth.loadSpecialities().then((res:any)=>{
        this.specialities=res;
        console.log(this.specialities)
    }).catch(err=>console.log(err))
  }
next(speciality){
  this.showType=true;
  this.speciality=speciality;

}
goForward(type){
  this.router.navigate(['/nurses'], {
    state: {
      speciality: JSON.stringify(this.speciality),
      type:type
    }
});
}
}
